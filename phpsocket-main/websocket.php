<?php

require __DIR__ . '/vendor/autoload.php';

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $input) {
        $input = json_decode($input, true);

        // через запрос в лару проверяем валидность токена
        $app = require __DIR__.'/bootstrap/app.php';
        $kernel = $app->make(\Illuminate\Contracts\Http\Kernel::class);
        $response = $kernel->handle(
            $request = \Illuminate\Http\Request::create('/api/user', 'GET', [], [], [], [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $input['token']
            ])
        );
        $controllerResult = $response->getContent();
        $kernel->terminate($request, $response);
        $user = json_decode($controllerResult, true);

        // формируем отправляемое сообщение
        $msg = [
            'from' => $user['email'],
            'content' => $input['content'],
        ];

        foreach ($this->clients as $client) {
            if ($from != $client) {
                $client->send(json_encode($msg));
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
}

$app = new Ratchet\App('localhost', 8080);
$app->route('/chat', new Chat, [ '*' ]);
echo "Server running at ws://127.0.0.1:8080" . PHP_EOL;
$app->run();
