<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Models\User;

class AuthController extends Controller
{
	public function signin (Request $request) {
		$credentials = [
			'email' => $request->input('email'),
			'password' => $request->input('password')
		];

		if (!auth()->validate($credentials)) {
			return ([ 'ok' => false ]);
		}

		auth()->attempt($credentials);

		$token = $request->user()->createToken('auth');

		return ([
			'token' => $token->plainTextToken,
		]);
	}

	public static function signup(RegisterRequest $request)
    {
        $user = User::create($request->validated());

        return ([ 'ok' => true ]);
    }
}
