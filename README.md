## reactPHP websocket chat

php: 8.1

ratchet: 0.4.4 (reactPHP)

laravel: 10.10

## установка и запуск

```
composer install
```

заполняем .env

```
php artisan migrate
```

запускаем laravel (для API входа, регистрации)
```
php artisan serve
```

запускаем websocket сервер:
```
php websocket.php
```