<!DOCTYPE html>
<html>
<head>
    <title>reactphp websocket chat</title>
    <style>
        .messages {
            height: 50vh;
            overflow: visible;
            width: 50vh;
        }
        .message {
            border: 1px solid;
        }
        .message .from {
            background: #ccc;
        }
    </style>
</head>
<body>

<form id="signin">
    <h2>sign in</h2>
    <input name="email" placeholder="email">
    <input type="password" name="password" placeholder="******">
    <input type="submit">
</form>

<form id="signup">
    <h2>sign up</h2>
    <input name="email" placeholder="email">
    <input type="password" name="password" placeholder="******">
    <input type="password" name="password_confirmation" placeholder="******">
    <input type="submit">
</form>

<div id="chat">
    <div class="messages"></div>
    <form id="send">
        <textarea placeholder="message"></textarea>
        <input type="submit">
    </form>
</div>

<template id="message">
    <div class="message">
        <div class="from"></div>
        <div class="content"></div>
    </div>
</template>

<script>
    let token = ''
    let socket = null

    const messagesEl = document.querySelector('.messages')

    function openChat () {
        socket = new WebSocket('ws://localhost:8080/chat')
        socket.onmessage = function(e) {
            const data = JSON.parse(e.data)
            console.log(data)
            showMessage(data.from, data.content)
        }
    }

    function sendMessage (content) {
        if (!socket || !token) {
            return
        }

        showMessage('ME', content)

        socket.send(JSON.stringify({
            token: token,
            content: content,
        }))
    }

    function showMessage (from, content) {
        let clone = document.querySelector('template#message')
            .content.cloneNode(true)

        clone.querySelector('.from')
            .innerText = from
        clone.querySelector('.content')
            .innerText = content

        messagesEl.append(clone)
    }

    document.querySelector('#signin')
        .addEventListener('submit', async (e) => {
            e.preventDefault()

            const form = new FormData()
            form.set('email', e.target.querySelector('[name="email"]').value)
            form.set('password', e.target.querySelector('[name="password"]').value)

            const raw = await fetch('/api/signin', {
                method: 'POST',
                body: form
            })

            const resp = await raw.json()

            if (!resp.token) {
                alert('auth error!')
            }

            token = resp.token

            openChat()
        })

    document.querySelector('#signup')
        .addEventListener('submit', async (e) => {
            e.preventDefault()

            const form = new FormData()
            form.set('email', e.target.querySelector('[name="email"]').value)
            form.set('password', e.target.querySelector('[name="password"]').value)
            form.set('password_confirmation', e.target.querySelector('[name="password_confirmation"]').value)

            const raw = await fetch('/api/signup', {
                method: 'POST',
                body: form
            })

            const resp = await raw.json()

            alert(resp.ok ? 'ok' : JSON.stringify(resp.errors, null, 2))
        })

    document.querySelector('#send')
        .addEventListener('submit', (e) => {
            e.preventDefault()
            
            const content = e.target.querySelector('textarea').value
            e.target.querySelector('textarea').value = ''

            sendMessage(content)
        })
</script>

</body>
</html>